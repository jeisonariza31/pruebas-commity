from django.urls import path
from .views import *

urlpatterns = [
    path('',  getAllUsers, name = 'users'),
    path('<int:pk>', find_by_id, name = 'user'),
    path('save/', newUser, name = 'new_user'),
    path('login/', login, name = 'login'),
    path('<int:pk>/update', updateUser, name = 'update_user'),
    path('<int:pk>/update-password', updatePassword, name = 'update_password'),
    path('<int:pk>/delete', deleteUser, name = 'delete_user'),
]
