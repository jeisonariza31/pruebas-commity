from rest_framework import serializers
from .models import *

#default serializer

class UsersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = '__all__'

# custom serializer
#----------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------------------------------
#inf user
class getInfUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ('name','last_name','user_name','email','created_at')

# new user serializer
class NewUserSerializer(serializers.ModelSerializer):

    name = serializers.CharField(max_length=300, required=True)
    last_name = serializers.CharField(max_length=300, required=True)
    user_name = serializers.CharField(max_length=300, required=True)
    email = serializers.EmailField(max_length=200, required=True)
    password = serializers.CharField(max_length=200, required=True)

    class Meta:
        model = Users
        fields = ['name', 'email', 'password','last_name','user_name']

# login serializer
class LoginSerializer(serializers.ModelSerializer):

    email = serializers.EmailField(max_length=200, required=True)
    password = serializers.CharField(max_length=200, required=True)

    class Meta:
        model = Users
        fields = ['email', 'password']

#update user serializer
class UpdateUserSerializer(serializers.ModelSerializer):
    
    name = serializers.CharField(max_length=300, required=True)
    last_name = serializers.CharField(max_length=300, required=True)
    user_name = serializers.CharField(max_length=300, required=True)
    email = serializers.EmailField(max_length=200, required=True)
    updated_at = serializers.DateTimeField(required=False)
    
    class Meta:
        model = Users
        fields = ['name', 'email','last_name','user_name','updated_at']

#update password serializer
class UpdatePasswordSerializer(serializers.ModelSerializer):
    
    password = serializers.CharField(max_length=200, required=True)
    
    class Meta:
        model = Users
        fields = ['password']



