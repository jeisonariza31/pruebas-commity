from django.db import models

# Create your models here.
class Users(models.Model):
    name = models.CharField(max_length=300)
    last_name = models.CharField(max_length=300)
    user_name = models.CharField(max_length=300, unique=True)
    status = models.CharField(max_length=1, default='A')
    email = models.EmailField(max_length=200, unique=True)
    password = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'users'