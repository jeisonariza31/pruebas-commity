from dataclasses import fields
from django.shortcuts import render

from .models import *
from .serializer import *
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.request import Request
import hashlib

# Create your views here.
@api_view(['GET'])
def getAllUsers(request: Request):
    if not request.query_params:
        users = Users.objects.all()
        serializer = UsersSerializer(users, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    else:
        data = request.query_params
        if data.get('name') is not None:
            users = Users.objects.filter(name__icontains=data.get('name'))
            
        elif data.get('last_name') is not None:
            users = Users.objects.filter(last_name__icontains=data.get('last_name'))

        elif data.get('user_name') is not None:
            users = Users.objects.filter(user_name__icontains=data.get('user_name'))

        elif data.get('email') is not None:
            users = Users.objects.filter(email__icontains=data.get('email'))

        serializer = UsersSerializer(users, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
        

@api_view(['GET'])
def find_by_id(request: Request, pk: int):
    try:
        user = Users.objects.get(pk=pk)
    except Users.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    serializer = getInfUserSerializer(user)
    return Response(serializer.data, status=status.HTTP_200_OK)
    
@api_view(['POST'])
def newUser(request: Request):
    email = request.data['email']
    user_name = request.data['user_name']
    existEmail = Users.objects.filter(email=email)
    existUser_name = Users.objects.filter(user_name=user_name)

    if existEmail.count() > 0:
        return Response({'message': 'El correo '+email+' ya se encuentra registrado'}, status=status.HTTP_400_BAD_REQUEST)
    
    if existUser_name.count() > 0:
        return Response({'message': 'El nombre de usuario '+user_name+' ya se encuentra registrado'}, status=status.HTTP_400_BAD_REQUEST)

    else:
        inf = request.data
        passwordEncode = hashlib.sha512(inf['password'].encode('utf-8')).hexdigest()
        inf.update({'password': passwordEncode})
        serializer = NewUserSerializer(data=inf) 
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def login(request: Request):
    email = request.data['email']
    password = request.data['password']
    passwordEncode = hashlib.sha512(password.encode('utf-8')).hexdigest()
    user = Users.objects.filter(email=email, password=passwordEncode)
    if user.count() > 0:
        serializer = UsersSerializer(user, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    else:
        return Response({'message': 'El correo o contraseña son incorrectos'}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['PUT'])
def updateUser(request: Request, pk: int):
    try:
        user = Users.objects.get(pk=pk)
    except Users.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    user_name=request.data['user_name']
    email = request.data['email']
    existUser = Users.objects.filter(user_name=user_name)
    existEmail = Users.objects.filter(email=email)
    if existUser.count() > 0:
        return Response({'message': 'El nombre de usuario ya se encuentra registrado en una cuenta'}, status=status.HTTP_400_BAD_REQUEST)
    if existEmail.count() > 0:
        return Response({'message': 'El correo ya se encuentra registrado en una cuenta'}, status=status.HTTP_400_BAD_REQUEST)
    else:
        serializer = UpdateUserSerializer(user, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['PATCH'])
def updatePassword(request: Request, pk: int):
    try:
        user = Users.objects.get(pk=pk)
    except Users.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    currentPassword = request.data['current_password']
    currentPasswordEncode = hashlib.sha512(currentPassword.encode('utf-8')).hexdigest()

    newPassword = request.data['new_password']
    confirmedPassword = request.data['confirmed_new_password']

    if user.password == currentPasswordEncode:
        if newPassword == confirmedPassword:
            newPasswordEncode = hashlib.sha512(newPassword.encode('utf-8')).hexdigest()
            user.password = newPasswordEncode
            user.save()
            return Response({'message': 'se actualizo la contrasena correctamente'},status=status.HTTP_200_OK)
        else:
            return Response({'message': 'Las contraseñas no coinciden'}, status=status.HTTP_400_BAD_REQUEST)
    else:
        return Response({'message': 'La contraseña actual es incorrecta'}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['DELETE'])
def deleteUser(request: Request, pk: int):
    try:
        user = Users.objects.get(pk=pk)
    except Users.DoesNotExist:
        return Response({'message': 'no se pudo encontrar al usuario'},status=status.HTTP_404_NOT_FOUND)
    user.delete()
    return Response({'message': 'se elimino el usuario correctamente'},status=status.HTTP_204_NO_CONTENT)